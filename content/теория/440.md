---
title: ВЕЧНАЯ ПЕНСИЯ
date: 2019-04-19
lastmod: 2022-08-19
url: теория/440
---
Мы согласились на беспредметные выражения -- «эффективно», «успешно»,
«современно» и тому подобные. Что именно -- нам не сказали, а мы не
спросили. Мы сами дали простор лукавству, то есть продаже души дьяволу,
но введение в искушение -- грех более тяжкий, чем само искушение. Мы
думаем, что мы сами отказались от всех благ социализма? Нет, всё гораздо
хуже -- мы отказались от блага самих себя. Теперь самая достойная,
эффективная и современная пенсия -- это пень с топором.

# ВЕЧНАЯ ПЕНСИЯ

Для того, чтобы люди получали зарплату, оплата жилья от которой
составляла 15 процентов, для того, чтобы старики и дети везде ездили
бесплатно и для того, чтобы в судах и парламенте заседали рабочие, сто
лет назад свергли и убили царя, потом разогнали буржуев и помещиков из
14 держав и из своей (15-ой). В итоге развязали себе руки, построили
индустрию, подняли целину, разбили зубы гитлеровской Европе, заставили
США мочить полосатые штаны за двумя океанами. Именно благодаря этому всё
и получилось, а не потому что ходили на работу. На работу ходили и при
царе. 

Потом почему-то решили, что надо опять жить как при царе, землю отдали
помещикам (некоторые из них кинорежиссёры), заводы буржуям и теперь
спохватились: а где же наша пенсия? Так ваши пенсионные накопления и
были вложены в эти земли и заводы. Вот вы на радость всем буржуям это
все и отдали. Восхищались как буржуи умеют зарабатывать? Так заработали
это всё -- вы! Теперь вы опять ходите на работу. Вы уже себе заработали
отсутствие пенсий, а сейчас вы ходите зарабатывать не пожизненный, а
наследственный долг банкам. Заметьте, тем самым банкам, в которых лежат
ваши деньги, потому что других денег не существует в природе. Можете
работать в две смены на них, тогда из ваших детей за долги будут резать
органы. Ни пенсии, ни зарплаты, ни квартиры не зарабатывают,
зарабатывают только «орден Сутулого». Это все завоёвывают! \

Кто для вас это всё завоёвывал: Ленин, Сталин, Берия, допишите фамилию
ваших прадедов если они воевали за то, чтобы у вас пенсия была, а не за
то, чтобы её не было -- вам они дураки, палачи, извращенцы, сумасшедшие,
а царь вам святой. Вот идите в церковь и молитесь этому вашему святому,
чтобы Бог вам послал среднюю продолжительность жизни в 32 года, 50-ти
процентную младенческую смертность, 12-ти часовой рабочий день (в лучшем
случае), повальный сифилис, туберкулез, малярию, смерть от простейшего
аппендицита или пульпита из-за полного отсутствия медицины и лекарств.
При царе Бог вам все это послал и сейчас пошлет, потому что Бог является
принципиальным противником царизма \[1\], требует установления диктатуры
пролетариата \[2\], и требует построения космического коммунизма \[3\].
А за что вы хотели получать пенсию, за предательство \[4\]?

Говорят, что «всех не пересажаете». На это можно ответить еще одним
расхожим выражением «нет ничего невозможного». Те, кто не сидят на
государственной зоне, а преуспели в жизни, строят персональные тюрьмы,
нанимают самую лучшую охрану, сидят внутри за решеткой и боятся.
Уголовная статья «измена родине» распространяется на всех и неотвратима.
Вот ваша пенсия за измену родине. Вы думали, что массовая измена -- это
не измена? Нет, это более тяжкое преступление. Вы все любите пугаться
страшного «числа зверя 666», вам нравится с ужасом в голосе пытаться
выяснить его истинное значение. И что означает принять «начертание на
чело свое, или на руку свою» \[5\]. Так вот Соломон получал в год 666
талантов золота налогов \[6\]. Это прямое, ясное и недвусмысленное
указание на финансовую систему, которое может быть непонятно только вам,
потому что вы приняли это «число» на чело своё и руку свою. То есть
думаете о деньгах, и делаете что-то за деньги и несёте свое кровавое
золото в храм, чтобы больше угодить бедному плотнику.

Те, кто позволил украсть или украл золото, предназначенное для божьих
контактов, будут кипеть ниже адского дна в расплавленном золоте. Те, кто
позволил украсть или украл вольфрам, предназначенный для божьих
двигателей, будут кипеть в расплавленном вольфраме. Те, кто украл или
позволил украсть данную Богом свободу, будут гореть в кипящей свободе.
Это для других будет страшный суд, для тех, кто совершил преступления
против людей. А те, кто бросил вызов промыслу божьему -- советской
промышленности будут о нём мечтать, но для них другого суда уже не
будет. По делам вашим будет вам и пенсия. Вечная.

\[1\] -- «А вы теперь отвергли Бога вашего, Который спасает вас от всех
бедствий ваших и скорбей ваших, и сказали Ему: "царя поставь над
нами". Итак предстаньте теперь пред Господом по коленам вашим и по
племенам вашим». (1 Цар. 10:19)

\[2\] -- «Потому сказываю вам, что отнимется от вас Царство Божие и дано
будет народу, приносящему плоды его».
(Мф. 21, 43-46)

\[3\] -- «Удобнее верблюду пройти сквозь игольные уши, нежели богатому
войти в Царствие Божие». (Мф. 19:24)

Царствие Божие -- царство небесное, это космическое государство, которое
смогло сконцентрировать энергию для того, чтобы преодолеть хотя бы
первую космическую скорость, а богатые не могут туда войти, потому что
частная собственность препятствует концентрации энергии, по этой причине
Советский Союз до сих пор остается единственной действительно
космической державой. 

\[4\] -- «Товарищ Бог»

http://russianwasteland.ru/теория/395/

\[5\] -- «И третий Ангел последовал за ними, говоря громким голосом: кто
поклоняется зверю и образу его и принимает начертание на чело свое, или
на руку свою, тот будет пить вино ярости Божией, вино цельное,
приготовленное в чаше гнева Его, и будет мучим в огне и сере пред
святыми Ангелами и пред Агнцем; и дым мучения их будет восходить во веки
веков, и не будут иметь покоя ни днем, ни ночью поклоняющиеся зверю и
образу его и принимающие начертание имени его. Здесь терпение святых,
соблюдающих заповеди Божии и веру в Иисуса» (Откр. 14:9-12).

«Весь мир насилья мы разрушим
До основанья...» 

«Интернационал»

\[6\] -- «В золоте, которое приходило Соломону в каждый год, весу было
шестьсот шестьдесят шесть талантов золотых» (3 Цар. 10:14)

Позиция Редакции

18 июля 2018 г.
